﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DynamicExpressions.AuxObjects;

namespace DynamicExpressions
{

    class CompoundExpression
    {

        #region Basic properties and structures of the CompoundExpression object

        public enum BooleanOperators
        {
            AND,
            OR
        }

        public class PartialExpressionStructure
        {
            public BooleanOperators? BooleanOperator;
            public List<BracketEntity> LeftBrackets;
            public SimpleExpression SimpleExpression;
            public List<BracketEntity> RightBrackets;
        }

        int HighestAllowedInputLevel = 15;

        private int ValidateLevelBracketLevel(int? UserInput)
        {
            int ValidatedUserLevel;

            if (UserInput < 0)
            {
                ValidatedUserLevel = 0;
                throw new ArgumentException("The level cannot be lower than zero. Input has been changed to zero.");
            }
            else if (UserInput > HighestAllowedInputLevel)
            {
                ValidatedUserLevel = HighestAllowedInputLevel;
                throw new ArgumentException("The level cannot be higher than " + HighestAllowedInputLevel + ". " + "Input has been changed to " + HighestAllowedInputLevel + ".");
            }
            else if (UserInput == null)
            {
                // Based on a premise that 0 and null have the same meaning in this case (levels constructed by a number of brackets on each side of a simple expression).
                ValidatedUserLevel = 0;
            }
            else
            {
                ValidatedUserLevel = (int)UserInput;
            }

            return ValidatedUserLevel;
        }

        public bool NormalizedExpression
        {
            get; protected set;
        }

        public List<PartialExpressionStructure> CompoundExpressionBody { get; private set; }

        #endregion


        #region Initialization and construction of the CompoundExpression object

        public CompoundExpression(SimpleExpression FirstExpression, int? StartBracketLevel = null, int? EndBracketLevel = null)
        {
            NormalizedExpression = false;

            CompoundExpressionBody = new List<PartialExpressionStructure>();

            StartBracketLevel = ValidateLevelBracketLevel(StartBracketLevel);
            EndBracketLevel = ValidateLevelBracketLevel(EndBracketLevel);

            List<BracketEntity> _LeftBrackets = new List<BracketEntity>();
            for (int x = 0; x != StartBracketLevel; x++)
            {
                _LeftBrackets.Add(new BracketEntity() { BracketSide = BracketEntity.BracketSides.Left, BracketType = BracketEntity.BracketTypes.Regular });
            }
            List<BracketEntity> _RightBrackets = new List<BracketEntity>();
            for (int x = 0; x != EndBracketLevel; x++)
            {
                _RightBrackets.Add(new BracketEntity() { BracketSide = BracketEntity.BracketSides.Right, BracketType = BracketEntity.BracketTypes.Regular });
            }

            PartialExpressionStructure PartialExpression = new PartialExpressionStructure()
            {
                LeftBrackets = _LeftBrackets,
                BooleanOperator = null,
                SimpleExpression = FirstExpression,
                RightBrackets = _RightBrackets
            };

            CompoundExpressionBody.Add(PartialExpression);
        }

        /// <summary>
        /// Bracket levels cannot be lower than 0 or higher than 15. Otherwise you'll get exception.
        /// </summary>
        public void AddExpression(BooleanOperators BooleanOperator, SimpleExpression NextExpression, int? StartBracketLevel = null, int? EndBracketLevel = null)
        {
            NormalizedExpression = false;

            StartBracketLevel = ValidateLevelBracketLevel(StartBracketLevel);
            EndBracketLevel = ValidateLevelBracketLevel(EndBracketLevel);

            List<BracketEntity> _LeftBrackets = new List<BracketEntity>();
            for (int x = 0; x != StartBracketLevel; x++)
            {
                _LeftBrackets.Add(new BracketEntity() { BracketSide = BracketEntity.BracketSides.Left, BracketType = BracketEntity.BracketTypes.Regular });
            }
            List<BracketEntity> _RightBrackets = new List<BracketEntity>();
            for (int x = 0; x != EndBracketLevel; x++)
            {
                _RightBrackets.Add(new BracketEntity() { BracketSide = BracketEntity.BracketSides.Right, BracketType = BracketEntity.BracketTypes.Regular });
            }

            PartialExpressionStructure PartialExpression = new PartialExpressionStructure()
            {
                LeftBrackets = _LeftBrackets,
                BooleanOperator = BooleanOperator,
                SimpleExpression = NextExpression,
                RightBrackets = _RightBrackets
            };

            CompoundExpressionBody.Add(PartialExpression);
        }

        #endregion


        #region Normalization of the CompoundExpression object: Completion of opened brackets and surrounding the expression with brackets.
        
        public void NormalizeExpression()
        {
            LevelAdjuster levelAdjuster = new LevelAdjuster(this);
            CompoundExpressionBody = levelAdjuster.NormalizeExpression();
            NormalizedExpression = levelAdjuster._NormalizationStatus;
        }

        #endregion


        #region Transciption of the compounded expression.

        public string Transcribe()
        {
            Transcriber transcriber = new Transcriber(this);
            return transcriber.Transcribe();
        }

        #endregion


        #region [NONEXISTENT!!!] Evaluation of the compound expression

        public void /*bool*/ EvaluateCompoundExpression()
        {
            bool? ExpressionResult = null;

            MessageBox.Show(ExpressionResult.ToString());

            //return ExpressionResult;
        }

        #endregion


    }

}
