﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace DynamicExpressions
{
    class SimpleExpression
    {

        private struct _ExpressionStructure
        {
            public bool Negace;
            public int FirstArgument;
            public RelationalOperators RelationalOperator;
            public int SecondArgument;
        }

        _ExpressionStructure _Expression;

        public SimpleExpression(int arg1, RelationalOperators Operator, int arg2, bool ExpressionNegation = false)
        {
            _Expression = new _ExpressionStructure() {
                Negace = ExpressionNegation,
                FirstArgument = arg1,
                RelationalOperator = Operator,
                SecondArgument = arg2
            };
        }


        public enum RelationalOperators
        {
            Lower,
            Larger,
            LowerOrEqual,
            LargerOrEqual,
            Equal
        };

        public bool EvaluateExpression()
        {
            bool ExpressionResult;

            switch (_Expression.RelationalOperator)
            {
                case RelationalOperators.Equal:
                    ExpressionResult = (_Expression.FirstArgument == _Expression.SecondArgument);
                    if (_Expression.Negace == true)
                    {
                        ExpressionResult = !ExpressionResult;

                    }
                    return ExpressionResult;


                case RelationalOperators.Larger:
                    ExpressionResult = (_Expression.FirstArgument > _Expression.SecondArgument);
                    if (_Expression.Negace == true)
                    {
                        ExpressionResult = !ExpressionResult;

                    }
                    return ExpressionResult;

                case RelationalOperators.LargerOrEqual:
                    ExpressionResult = (_Expression.FirstArgument >= _Expression.SecondArgument);
                    if (_Expression.Negace == true)
                    {
                        ExpressionResult = !ExpressionResult;

                    }
                    return ExpressionResult;

                case RelationalOperators.Lower:
                    ExpressionResult = (_Expression.FirstArgument < _Expression.SecondArgument);
                    if (_Expression.Negace == true)
                    {
                        ExpressionResult = !ExpressionResult;

                    }
                    return ExpressionResult;

                case RelationalOperators.LowerOrEqual:
                    ExpressionResult = (_Expression.FirstArgument <= _Expression.SecondArgument);
                    if (_Expression.Negace == true)
                    {
                        ExpressionResult = !ExpressionResult;

                    }
                    return ExpressionResult;

            }

            throw new Exception("Byl vybrán operátor, který nebyl ve switch výše zohledněn."); // Možná by se to spíše mělo napsat tak, aby toto vůbec nemohlo nastat?
        }

        public string Transcription()
        {

            string VyslednyPrepis;

            switch (_Expression.RelationalOperator)
            {
                case RelationalOperators.Equal:
                    VyslednyPrepis = "(" + (_Expression.FirstArgument + " == " + _Expression.SecondArgument) + ")";
                    if (_Expression.Negace == true)
                    {
                        VyslednyPrepis = "!" + VyslednyPrepis;
                    }
                    return VyslednyPrepis;

                case RelationalOperators.Larger:
                    VyslednyPrepis = "(" + (_Expression.FirstArgument + " > " + _Expression.SecondArgument) + ")";
                    if (_Expression.Negace == true)
                    {
                        VyslednyPrepis = "!" + VyslednyPrepis;
                    }
                    return VyslednyPrepis;

                case RelationalOperators.LargerOrEqual:
                    VyslednyPrepis = "(" + (_Expression.FirstArgument + " >= " + _Expression.SecondArgument) + ")";
                    if (_Expression.Negace == true)
                    {
                        VyslednyPrepis = "!" + VyslednyPrepis;
                    }
                    return VyslednyPrepis;

                case RelationalOperators.Lower:
                    VyslednyPrepis = "(" + (_Expression.FirstArgument + " < " + _Expression.SecondArgument) + ")";
                    if (_Expression.Negace == true)
                    {
                        VyslednyPrepis = "!" + VyslednyPrepis;
                    }
                    return VyslednyPrepis;

                case RelationalOperators.LowerOrEqual:
                    VyslednyPrepis = "(" + (_Expression.FirstArgument + " <= " + _Expression.SecondArgument) + ")";
                    if (_Expression.Negace == true)
                    {
                        VyslednyPrepis = "!" + VyslednyPrepis;
                    }
                    return VyslednyPrepis;
            }

            throw new Exception("Byl vybrán operátor, který nebyl ve switch výše zohledněn.");


        }
    }
}
