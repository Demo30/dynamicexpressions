﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DynamicExpressions
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        //DEBUG
        //struct lvl_debug
        //{
        //    public int? start;
        //    public int? end;
        //}
        //List<lvl_debug> level_debug = new List<lvl_debug>();


        public void TryOut()
        {

            //DEBUG
            //level_debug.Add(new lvl_debug { start = 1, end = null });
            //level_debug.Add(new lvl_debug { start = null, end = 2 });
            //level_debug.Add(new lvl_debug { start = 5, end = 3 });
            //level_debug.Add(new lvl_debug { start = 0, end = 0 });
            //level_debug.Add(new lvl_debug { start = 5, end = 0 });
            //level_debug.Add(new lvl_debug { start = 0, end = 2 });
            //level_debug.Add(new lvl_debug { start = 1, end = 1 });
            //level_debug.Add(new lvl_debug { start = 0, end = 1 });


            Random PanNahoda = new Random();

            Array _OperatorValues = Enum.GetValues(typeof(SimpleExpression.RelationalOperators));
            bool[] _NegaceValues = new bool[] { true, false };

            bool _Negace = _NegaceValues[PanNahoda.Next(2)];
            int _A_arg1 = PanNahoda.Next(-50, 50);
            SimpleExpression.RelationalOperators _A_Operator = (SimpleExpression.RelationalOperators)_OperatorValues.GetValue(PanNahoda.Next(_OperatorValues.Length));
            int _A_arg2 = PanNahoda.Next(-50, 50);

            SimpleExpression SimpleExpression = new SimpleExpression(_A_arg1, _A_Operator, _A_arg2, _Negace);


            int? StartBrackets = NahodnyLevel(PanNahoda);
            int? EndBrackets = NahodnyLevel(PanNahoda);

            //DEBUG
            //StartBrackets = level_debug[0].start;
            //EndBrackets = level_debug[0].end;

            CompoundExpression CompoundExpression = new CompoundExpression(SimpleExpression, StartBrackets, EndBrackets);

            for (int x = 1; x < 8; x++)
            {
                bool[] _NegaceValues_x = new bool[] { true, false };

                bool _Negace_x = _NegaceValues_x[PanNahoda.Next(_NegaceValues_x.Length)];
                int _A_arg1_x = PanNahoda.Next(-50, 50);
                SimpleExpression.RelationalOperators _A_Operator_x = (SimpleExpression.RelationalOperators)PanNahoda.Next(typeof(SimpleExpression.RelationalOperators).GetEnumValues().Length);
                int _A_arg2_x = PanNahoda.Next(-50, 50);

                SimpleExpression SimpleExpression_x = new SimpleExpression(_A_arg1_x, _A_Operator_x, _A_arg2_x, _Negace_x);

                StartBrackets = NahodnyLevel(PanNahoda);
                EndBrackets = NahodnyLevel(PanNahoda);

                //DEBUG
                //int debug = x;
                //StartBrackets = level_debug[debug].start;
                //EndBrackets = level_debug[debug].end;

                CompoundExpression.BooleanOperators _CompoundExpressionOperator = (CompoundExpression.BooleanOperators)PanNahoda.Next(typeof(CompoundExpression.BooleanOperators).GetEnumValues().Length);

                CompoundExpression.AddExpression(_CompoundExpressionOperator, SimpleExpression_x, StartBrackets, EndBrackets);

            }

            label1_title.Content = SimpleExpression.Transcription();
            label1.Content = (SimpleExpression.EvaluateExpression()).ToString();


            CompoundExpression.NormalizeExpression();
            //CompoundExpression.EvaluateCompoundExpression();

            label2.Content = CompoundExpression.Transcribe();

        }

        private int? NahodnyLevel(Random PanNahoda)
        {
            int? Vysledek;

            Vysledek = PanNahoda.Next(-1, 3);

            if (Vysledek == -1)
            {
                Vysledek = null;
            }

            return Vysledek;
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            TryOut();
        }
    }
}
