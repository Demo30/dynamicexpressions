﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DynamicExpressions.AuxObjects
{
    static class GeneralAuxClass
    {
        public static int NullToZero(this int? PotentialNullIntValue)
        {
            if (PotentialNullIntValue == null) { return 0; }
            else { return (int)PotentialNullIntValue; }
        }

        public static int CountOccurencesOfBracketType(this List<BracketEntity> Brackets, List<BracketEntity.BracketTypes> JustChosenBracketType = null)
        {
            int count = 0;
            foreach (BracketEntity Bracket in Brackets)
            {
                if (Bracket.BracketType == BracketEntity.BracketTypes.Regular && (JustChosenBracketType == null || JustChosenBracketType.Contains(BracketEntity.BracketTypes.Regular)))
                {
                    count++;
                }
                else if (Bracket.BracketType == BracketEntity.BracketTypes.NormalizationBrackets && (JustChosenBracketType == null || JustChosenBracketType.Contains(BracketEntity.BracketTypes.NormalizationBrackets)))
                {
                    count++;
                }
                else if (Bracket.BracketType == BracketEntity.BracketTypes.OrLevelBracket && (JustChosenBracketType == null || JustChosenBracketType.Contains(BracketEntity.BracketTypes.OrLevelBracket)))
                {
                    count++;
                }
            }
            return count;
        }
    }
}
