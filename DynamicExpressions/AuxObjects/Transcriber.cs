﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace DynamicExpressions.AuxObjects
{
    class Transcriber
    {
        private List<CompoundExpression.PartialExpressionStructure> _CompoundExpressionBody4Transcription { get; set; }
        private bool _NormalizationStatus { get; set; }

        public Transcriber(CompoundExpression CompoundExpressionBody4Transcription)
        {
            _CompoundExpressionBody4Transcription = CompoundExpressionBody4Transcription.CompoundExpressionBody;
            _NormalizationStatus = CompoundExpressionBody4Transcription.NormalizedExpression;
        }

        public string Transcribe()
        {
            if (_NormalizationStatus == false)
            {
                return "Compounded expression has not been normalized. Use NormalizeExpression() method to complete the expression first.";
            }

            string VyslednyPrepis = "";

            foreach (CompoundExpression.PartialExpressionStructure PartialExpression in _CompoundExpressionBody4Transcription)
            {
                string PrepisOperatoru = "";

                switch (PartialExpression.BooleanOperator)
                {
                    case CompoundExpression.BooleanOperators.AND: PrepisOperatoru = "&&"; break;
                    case CompoundExpression.BooleanOperators.OR: PrepisOperatoru = "||"; break;
                    default: PrepisOperatoru = ""; break;
                }

                List<BracketEntity.BracketTypes> RegularAndOrBrackets = new List<BracketEntity.BracketTypes>() {
                    BracketEntity.BracketTypes.Regular,
                    BracketEntity.BracketTypes.OrLevelBracket
                };

                string LeftBrackets = WriteBrackets(PartialExpression.LeftBrackets);
                string RightBrackets = WriteBrackets(PartialExpression.RightBrackets);

                VyslednyPrepis += " " + PrepisOperatoru + " " + LeftBrackets + PartialExpression.SimpleExpression.EvaluateExpression() + RightBrackets;
            }

            VyslednyPrepis = VyslednyPrepis.Trim();

            return VyslednyPrepis;
        }

        private string WriteBrackets(List<BracketEntity> Brackets, BracketEntity.BracketTypes? JustChosenType = null)
        {
            string TranscriptionOutput = "";

            foreach (BracketEntity Bracket in Brackets)
            {
                if (Bracket.BracketType == BracketEntity.BracketTypes.Regular && (JustChosenType == null || JustChosenType == BracketEntity.BracketTypes.Regular))
                {
                    if (Bracket.BracketSide == BracketEntity.BracketSides.Left) { TranscriptionOutput += "(" + Bracket.Level; }
                    else if (Bracket.BracketSide == BracketEntity.BracketSides.Right) { TranscriptionOutput += Bracket.Level + ")"; }
                }
                else if (Bracket.BracketType == BracketEntity.BracketTypes.NormalizationBrackets && (JustChosenType == null || JustChosenType == BracketEntity.BracketTypes.NormalizationBrackets))
                {
                    if (Bracket.BracketSide == BracketEntity.BracketSides.Left) { TranscriptionOutput += "{" + Bracket.Level; }
                    else if (Bracket.BracketSide == BracketEntity.BracketSides.Right) { TranscriptionOutput += Bracket.Level + "}"; }
                }
                else if (Bracket.BracketType == BracketEntity.BracketTypes.OrLevelBracket && (JustChosenType == null || JustChosenType == BracketEntity.BracketTypes.OrLevelBracket))
                {
                    if (Bracket.BracketSide == BracketEntity.BracketSides.Left) { TranscriptionOutput += "[" + Bracket.Level; }
                    else if (Bracket.BracketSide == BracketEntity.BracketSides.Right) { TranscriptionOutput += Bracket.Level + "]"; }
                }
            }

            return TranscriptionOutput;
        }
    }
}
