﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DynamicExpressions.AuxObjects
{
    class BracketEntity
    {
        public enum BracketSides
        {
            None,
            Left,
            Right
        }

        public enum BracketTypes
        {
            General,
            Regular,
            OrLevelBracket,
            NormalizationBrackets
        }

        public BracketTypes BracketType { get; set; }
        public BracketSides BracketSide { get; set; }
        public int Level { get; set; }
    }
}
