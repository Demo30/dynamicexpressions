﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;


namespace DynamicExpressions.AuxObjects
{
    class LevelAdjuster
    {
        public bool _NormalizationStatus;

        struct FinalBracketCountStruct
        {
            public int LeftBrackets;
            public int RightBracket;
        }

        List<CompoundExpression.PartialExpressionStructure> _CompoundExpressionBody4Completion { get; set; }

        List<PartialCountStruct> _SequenceOfPartialDifferences = new List<PartialCountStruct>();

        public LevelAdjuster(CompoundExpression CompoundExpressionBody4Completion)
        {
            _CompoundExpressionBody4Completion = CompoundExpressionBody4Completion.CompoundExpressionBody;
            _NormalizationStatus = CompoundExpressionBody4Completion.NormalizedExpression;
            _SequenceOfPartialDifferences = CountPartialBracketDifferences();
        }

        private List<PartialCountStruct> CountPartialBracketDifferences()
        {
            List<PartialCountStruct> SequenceOfPartialDifferences = new List<PartialCountStruct>();

            foreach (CompoundExpression.PartialExpressionStructure PartialExpression in _CompoundExpressionBody4Completion)
            {
                int LeftPartialSum = PartialExpression.LeftBrackets.CountOccurencesOfBracketType(new List<BracketEntity.BracketTypes>() { BracketEntity.BracketTypes.Regular });
                int RightPartialSum = PartialExpression.RightBrackets.CountOccurencesOfBracketType(new List<BracketEntity.BracketTypes>() { BracketEntity.BracketTypes.Regular });

                PartialCountStruct PartialSolution = new PartialCountStruct();

                int Difference = LeftPartialSum - RightPartialSum;
                if (Difference > 0)
                {
                    PartialSolution.BracketCount = Difference;
                    PartialSolution.FinalBracketsSide = BracketEntity.BracketSides.Left;
                }
                else if (Difference < 0)
                {
                    PartialSolution.BracketCount = Math.Abs(Difference);
                    PartialSolution.FinalBracketsSide = BracketEntity.BracketSides.Right;
                }
                else if (Difference == 0)
                {
                    PartialSolution.BracketCount = 0;
                    PartialSolution.FinalBracketsSide = BracketEntity.BracketSides.None;
                }

                SequenceOfPartialDifferences.Add(PartialSolution);
            }

            return SequenceOfPartialDifferences;
        }

        public List<CompoundExpression.PartialExpressionStructure> NormalizeExpression()
        {
            if (_NormalizationStatus == true) { return _CompoundExpressionBody4Completion; };

            CompleteOpenedBrackets();

            OrBracketsAnalyzer OrLevelAnalyzer = new OrBracketsAnalyzer(_CompoundExpressionBody4Completion);
            _CompoundExpressionBody4Completion = OrLevelAnalyzer.AddOrLevelBrackets();

            AnalyzeLevels();

            _NormalizationStatus = true;
            return _CompoundExpressionBody4Completion;
        }

        private void CompleteOpenedBrackets()
        {
            FinalBracketCountStruct ClosingBrackets = new FinalBracketCountStruct();
            ClosingBrackets = CountOpenedBrackets(_SequenceOfPartialDifferences);

            for (int x = 0; x != ClosingBrackets.LeftBrackets; x++)
            {
                _CompoundExpressionBody4Completion[0].LeftBrackets.Insert(0, new BracketEntity { BracketSide = BracketEntity.BracketSides.Left, BracketType = BracketEntity.BracketTypes.NormalizationBrackets });
            }
            for (int x = 0; x != ClosingBrackets.RightBracket; x++)
            {
                _CompoundExpressionBody4Completion[_CompoundExpressionBody4Completion.Count - 1].RightBrackets.Add(new BracketEntity { BracketSide = BracketEntity.BracketSides.Right, BracketType = BracketEntity.BracketTypes.NormalizationBrackets });
            }
        }

        // !!!!!!!!!!!!!!!!!!!!!!!!!! // CELÉ TYTO SLOŽITÉ PODMÍNKY LZE MOŽNÁ ZREDUKOVAT NA DOCELA SNADNÝ VÝPOČET ZÁVOREK!! // ZKUSIT TO APLIKOVAT // !!!!!!!!!!!!!!!!!!!!!!!!!!
        private FinalBracketCountStruct CountOpenedBrackets(List<PartialCountStruct> SequenceOfPartialDifferences)
        {
            FinalBracketCountStruct Solution = new FinalBracketCountStruct();

            PartialCountStruct PartialCount = new PartialCountStruct()
            {
                BracketCount = 0,
                FinalBracketsSide = BracketEntity.BracketSides.None  
            };

            int OverallCountLeft = 0;
            int OverallCountRight = 0;

            int PartialSolutionsTrackCount = 0;

            foreach (PartialCountStruct CurrentPartialSolution in SequenceOfPartialDifferences)
            {
                PartialSolutionsTrackCount++;

                // 1st PartialSolution in sequence
                if (PartialSolutionsTrackCount == 1)
                {
                    PartialCount = CurrentPartialSolution;
                    continue;
                }
                // last PartialSolution in sequence
                else if (PartialSolutionsTrackCount == SequenceOfPartialDifferences.Count)
                {
                    // if the last PartialSolution is 0/None
                    if (CurrentPartialSolution.FinalBracketsSide == BracketEntity.BracketSides.None)
                    {
                        if (PartialCount.FinalBracketsSide == BracketEntity.BracketSides.Left)
                        {
                            OverallCountLeft += PartialCount.BracketCount;
                        }
                        else if (PartialCount.FinalBracketsSide == BracketEntity.BracketSides.Right)
                        {
                            OverallCountRight += PartialCount.BracketCount;
                        }
                    }
                    // if the last PartialSolution is not 0/None
                    else
                    {
                        // if PartialCount is not 0/None
                        if (PartialCount.FinalBracketsSide != BracketEntity.BracketSides.None)
                        {
                            if (PartialCount.FinalBracketsSide == CurrentPartialSolution.FinalBracketsSide)
                            {
                                int tempCount = PartialCount.BracketCount + CurrentPartialSolution.BracketCount;
                                if (CurrentPartialSolution.FinalBracketsSide == BracketEntity.BracketSides.Left)
                                {
                                    OverallCountLeft += tempCount;
                                }
                                else if (CurrentPartialSolution.FinalBracketsSide == BracketEntity.BracketSides.Right)
                                {
                                    OverallCountRight += tempCount;
                                }
                            }
                            else if (PartialCount.FinalBracketsSide == BracketEntity.BracketSides.Left && CurrentPartialSolution.FinalBracketsSide == BracketEntity.BracketSides.Right)
                            {
                                int tempCount = PartialCount.BracketCount - CurrentPartialSolution.BracketCount;
                                if (tempCount > 0)
                                {
                                    OverallCountLeft += tempCount;
                                }
                                else if (tempCount < 0)
                                {
                                    OverallCountRight += Math.Abs(tempCount);
                                }
                            }
                            else if (PartialCount.FinalBracketsSide == BracketEntity.BracketSides.Right && CurrentPartialSolution.FinalBracketsSide == BracketEntity.BracketSides.Left)
                            {
                                OverallCountRight += PartialCount.BracketCount;
                                OverallCountLeft += CurrentPartialSolution.BracketCount;
                            }
                        }
                        // if PartialCount is 0/None
                        else if (PartialCount.FinalBracketsSide == BracketEntity.BracketSides.None)
                        {
                            if (CurrentPartialSolution.FinalBracketsSide == BracketEntity.BracketSides.Left)
                            {
                                OverallCountLeft += CurrentPartialSolution.BracketCount;
                            }
                            else if (CurrentPartialSolution.FinalBracketsSide == BracketEntity.BracketSides.Right)
                            {
                                OverallCountRight += CurrentPartialSolution.BracketCount;
                            }
                        }
                    }
                }
                else if (CurrentPartialSolution.BracketCount == 0)
                {
                    continue;
                }
                else if (PartialCount.FinalBracketsSide == BracketEntity.BracketSides.None)
                {
                    PartialCount.FinalBracketsSide = CurrentPartialSolution.FinalBracketsSide;
                    PartialCount.BracketCount = CurrentPartialSolution.BracketCount;
                }
                else if (PartialCount.FinalBracketsSide == CurrentPartialSolution.FinalBracketsSide)
                {
                    PartialCount.BracketCount += CurrentPartialSolution.BracketCount;
                }
                else if (PartialCount.FinalBracketsSide == BracketEntity.BracketSides.Right && CurrentPartialSolution.FinalBracketsSide == BracketEntity.BracketSides.Left)
                {
                    OverallCountRight += PartialCount.BracketCount;
                    PartialCount.BracketCount = CurrentPartialSolution.BracketCount;
                    PartialCount.FinalBracketsSide = BracketEntity.BracketSides.Left;
                }
                else if (PartialCount.FinalBracketsSide == BracketEntity.BracketSides.Left && CurrentPartialSolution.FinalBracketsSide == BracketEntity.BracketSides.Right)
                {
                    int tempCount = PartialCount.BracketCount - CurrentPartialSolution.BracketCount;
                    if (tempCount > 0)
                    {
                        PartialCount.FinalBracketsSide = BracketEntity.BracketSides.Left;
                        PartialCount.BracketCount = tempCount;
                    }
                    else if (tempCount < 0)
                    {
                        PartialCount.FinalBracketsSide = BracketEntity.BracketSides.Right;
                        PartialCount.BracketCount = Math.Abs(tempCount);
                    }
                    else
                    {
                        PartialCount.FinalBracketsSide = BracketEntity.BracketSides.None;
                        PartialCount.BracketCount = 0;
                    }
                }
            }

            // The counts are saved to the opposite side because we need to complete those extra brackets that we've just counted
            Solution.LeftBrackets = OverallCountRight;
            Solution.RightBracket = OverallCountLeft;

            return Solution;
        }

        struct PartialCountStruct
        {
            public BracketEntity.BracketSides FinalBracketsSide { get; set; }
            public int BracketCount { get; set; }
        }

        class OrBracketsAnalyzer
        {
            List<CompoundExpression.PartialExpressionStructure> _CompoundExpressionBody4Completion;
            List<int> OrPositionsWithinCompoundExpression = new List<int>();

            public OrBracketsAnalyzer(List<CompoundExpression.PartialExpressionStructure> CompoundExpressionBody)
            {
                _CompoundExpressionBody4Completion = CompoundExpressionBody;
            }

            public List<CompoundExpression.PartialExpressionStructure> AddOrLevelBrackets()
            {
                LocateOrOperators();
                AddBaseOrLevelBrackets();
                CompleteOpenedOrLevelBrackets();
                return _CompoundExpressionBody4Completion;
            }

            private void LocateOrOperators()
            {
                int Tracker = 0;
                foreach (CompoundExpression.PartialExpressionStructure PartialExpression in _CompoundExpressionBody4Completion)
                {
                    if (PartialExpression.BooleanOperator == CompoundExpression.BooleanOperators.OR) { OrPositionsWithinCompoundExpression.Add(Tracker); }
                    Tracker++;
                }
            }

            private void AddBaseOrLevelBrackets()
            {
                foreach (int OrPosition in OrPositionsWithinCompoundExpression)
                {
                    _CompoundExpressionBody4Completion[OrPosition - 1].RightBrackets.Add(new BracketEntity() {
                        BracketSide = BracketEntity.BracketSides.Right,
                        BracketType = BracketEntity.BracketTypes.OrLevelBracket
                    });
                    _CompoundExpressionBody4Completion[OrPosition].LeftBrackets.Insert(0, new BracketEntity() {
                        BracketSide = BracketEntity.BracketSides.Left,
                        BracketType = BracketEntity.BracketTypes.OrLevelBracket
                    });
                }
            }

            private void CompleteOpenedOrLevelBrackets()
            {
                int IterationTracker = 0;

                foreach (int CurrentOrPosition in OrPositionsWithinCompoundExpression)
                {
                    #region Left OrBrackets completion

                    bool DoneWithThisIteration = false;
                    int PartialCount = 0;

                    for (int x = (CurrentOrPosition - 1); x >= 0 && DoneWithThisIteration != true; x--)
                    {
                        PartialCount += _CompoundExpressionBody4Completion[x].RightBrackets.CountOccurencesOfBracketType();
                        PartialCount -= _CompoundExpressionBody4Completion[x].LeftBrackets.CountOccurencesOfBracketType();

                        if (PartialCount <= 0)
                        {
                            DoneWithThisIteration = true;

                            int OrBracketPosition = Math.Abs(PartialCount) + 1;

                            _CompoundExpressionBody4Completion[x].LeftBrackets.Insert(OrBracketPosition, new BracketEntity() {
                                BracketSide = BracketEntity.BracketSides.Left,
                                BracketType = BracketEntity.BracketTypes.OrLevelBracket
                            });
                        }
                        else if (x == 0 && PartialCount != 0)
                        {
                            DoneWithThisIteration = true;

                            _CompoundExpressionBody4Completion[0].LeftBrackets.Insert(0, new BracketEntity {
                                BracketSide = BracketEntity.BracketSides.Left,
                                BracketType = BracketEntity.BracketTypes.OrLevelBracket
                            });
                        }
                    }

                    #endregion

                    #region Right OrBrackets completion

                    DoneWithThisIteration = false;
                    PartialCount = 0;

                    for (int y = CurrentOrPosition; y <= (_CompoundExpressionBody4Completion.Count() - 1) && DoneWithThisIteration != true; y++)
                    {
                        PartialCount += _CompoundExpressionBody4Completion[y].LeftBrackets.CountOccurencesOfBracketType();
                        PartialCount -= _CompoundExpressionBody4Completion[y].RightBrackets.CountOccurencesOfBracketType();

                        if (PartialCount <= 0)
                        {
                            DoneWithThisIteration = true;

                            int OrBracketPosition = (_CompoundExpressionBody4Completion[y].RightBrackets.CountOccurencesOfBracketType() - Math.Abs(PartialCount)) - 1;
                            _CompoundExpressionBody4Completion[y].RightBrackets.Insert(OrBracketPosition, new BracketEntity()
                            {
                                BracketSide = BracketEntity.BracketSides.Right,
                                BracketType = BracketEntity.BracketTypes.OrLevelBracket
                            });
                        }
                        else if (y == (_CompoundExpressionBody4Completion.Count() - 1) && PartialCount != 0)
                        {
                            DoneWithThisIteration = true;

                            _CompoundExpressionBody4Completion[_CompoundExpressionBody4Completion.Count - 1].RightBrackets.Add(new BracketEntity()
                            {
                                BracketSide = BracketEntity.BracketSides.Right,
                                BracketType = BracketEntity.BracketTypes.OrLevelBracket
                            });
                        }
                    }
                    #endregion

                    IterationTracker++;
                }
            }


        }

        private void AnalyzeLevels()
        {
            // doplnění informací o levelech nějak

            int PartialExpressionsIterationTracker = 0;
            int Levels = 0;

            foreach (CompoundExpression.PartialExpressionStructure PartialExpression in _CompoundExpressionBody4Completion)
            {
                int BracketIterationTracker = 0;
                foreach (BracketEntity LeftBrackets in PartialExpression.LeftBrackets)
                {
                    Levels += 1;
                    LeftBrackets.Level = Levels;
                    BracketIterationTracker++;
                }

                BracketIterationTracker = 0;
                foreach (BracketEntity RightBrackets in PartialExpression.RightBrackets)
                {
                    RightBrackets.Level = Levels;
                    Levels -= 1;
                    BracketIterationTracker++;
                }

                PartialExpressionsIterationTracker++;
            }
        }

    }
}
